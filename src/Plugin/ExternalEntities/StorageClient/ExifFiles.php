<?php

namespace Drupal\xnttexif\Plugin\ExternalEntities\StorageClient;

use Drupal\Core\Cache\CacheBackendInterface;
use Drupal\Core\Config\ConfigFactory;
use Drupal\Core\Entity\EntityFieldManagerInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Logger\LoggerChannelFactoryInterface;
use Drupal\Core\Messenger\MessengerInterface;
use Drupal\Core\StringTranslation\TranslationInterface;
use Drupal\external_entities\Plugin\ExternalEntities\StorageClient\Files;

/**
 * External entities storage client for image files.
 *
 * @StorageClient(
 *   id = "xnttexif",
 *   label = @Translation("Image Files with exif data"),
 *   description = @Translation("Retrieves exif data from image files.")
 * )
 */
class ExifFiles extends Files {

  /**
   * {@inheritdoc}
   */
  public function __construct(
    array $configuration,
    $plugin_id,
    $plugin_definition,
    TranslationInterface $string_translation,
    LoggerChannelFactoryInterface $logger_factory,
    EntityTypeManagerInterface $entity_type_manager,
    EntityFieldManagerInterface $entity_field_manager,
    ConfigFactory $config_factory,
    MessengerInterface $messenger,
    CacheBackendInterface $cache
  ) {
    parent::__construct(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $string_translation,
      $logger_factory,
      $entity_type_manager,
      $entity_field_manager,
      $config_factory,
      $messenger,
      $cache
    );
    // Defaults.
    $this->fileType = $this->t('image');
    $this->fileTypePlural = $this->t('images');
    $this->fileTypeCap = $this->t('Image');
    $this->fileTypeCapPlural = $this->t('Images');
    // Default to JPEG but it could be PNG, TIFF, etc.
    $this->fileExtensions = ['.jpg', '.jpeg', '.png', '.tif', '.tiff', '.wepb'];
  }

  /**
   * {@inheritdoc}
   */
  protected function parseFile(string $file_path) :array {
    $data = parent::parseFile($file_path);
    $id = array_keys($data)[0];
    // Add Exif infos.
    if (function_exists('exif_read_data')) {
      $messenger = $this->messenger;
      set_error_handler(
        function (
          int $errno,
          string $errstr,
          string $errfile = '',
          int $errline = 0,
          array $errcontext = []
        ) use (
          $file_path,
          $messenger
        ) :bool {
          if (E_WARNING == $errno) {
            $messenger->addWarning('Exif data (' . $file_path . '): ' . $errstr);
            return TRUE;
          }
          elseif (E_NOTICE == $errno) {
            $messenger->addStatus('Exif data (' . $file_path . '): ' . $errstr);
            return TRUE;
          }
          return FALSE;
        },
        E_WARNING | E_NOTICE
      );
      $exif_data = exif_read_data($file_path, '', TRUE, TRUE) ?: [];
      restore_error_handler();
      // Flatten array.
      $flatten_array = function ($array, $prefix = '') use (&$flatten_array) {
        $flattened = [];
        foreach ($array as $key => $value) {
          if (is_array($value)) {
            $flattened += $flatten_array($value, $prefix . $key . '.');
          }
          else {
            $flattened[$prefix . $key] = $value;
          }
        }
        return $flattened;
      };
      $data[$id] += $flatten_array($exif_data);
    }
    else {
      $this->messenger->addWarning(
        $this->t(
          'Unable to load exif data: PHP exif extension is not installed or enabled.'
        )
      );
      $this->logger->warning(
        'Unable to load exif data from file "'
        . $file_path
        . '": PHP exif extension is not installed or enabled.'
      );
    }
    return $data;
  }

}
